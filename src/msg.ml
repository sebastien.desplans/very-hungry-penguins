(** Message module *)

type move = int (* TODO: change this *)

(** requests the client can do to the server *)
type to_server =
    GetListOfGames
  | NewGame of string * string * (string list) * int
  (** game_name,map_filename(json or txt),
      player_type (human, IA, ...) list, turn to feed Game_state module *)
  | GetGameState
  | Move of move (** TODO: type move should include a penguin to move ? *)
  | Disconnected

(** msg the server can send to the client ---
    ListOfGames: name of the game, name of the map, host, current nb of players,
    max nb of players, and password protection *)
type to_client =
    ListOfGames of (string * string * string * int * int * bool) list
  | GameState of int (* TODO: change thiiis *)
  | YourNumIs of int (* TODO: int ? *)
